"""Таблицы Acme Packet, содержащие инфо о протоколе SIP."""

import re

from acme.helpers import chunk, normalize_attr


class TableDoesNotExist(Exception):
    """Таблица не найдена."""


class Column:
    """Столбец таблицы."""

    def __init__(self, subcols, values):
        """Init."""

        for key, value in dict(zip(subcols, values)).items():
            setattr(self, key, value)


class Row:
    """Строка таблицы."""

    def __init__(self, cols, subcols, values):
        """Init."""

        for x in range(len(cols)):
            setattr(self, cols[x], Column(list(chunk(3, subcols))[x], values[x]))


class Table:
    """Таблица."""

    head_len = 0
    footer_len = 0
    cols = ()
    subcols = ()

    def __init__(self, data):
        """Init."""
        self.data = self.get_data(data)

        if self.data:
            for key, value in self.get_attrs().items():
                if key[0].isdigit():
                    key = '_%s' % (key)
                setattr(self, key, Row(self.cols, self.subcols, value))

    def get_raw_lines(self, raw_data):
        """Возвращает массив сырых строк."""
        raw_lines = [x.strip() for x in raw_data]

        if 'NO DATA AVAILABLE' in raw_lines[0]:
            raise TableDoesNotExist

        return raw_lines

    def get_data(self, raw_data):
        """Возвращает массив обработанных строк."""
        try:
            raw_lines = self.get_raw_lines(raw_data)
        except TableDoesNotExist:
            return []

        if self.footer_len > 0:
            return raw_lines[self.head_len:-self.footer_len]
        return raw_lines[self.head_len:]

    def get_attrs(self):
        """Возвращает аттрибуты класса."""
        attrs = {}
        for line in self.data:
            m = re.search(r'^(.*?)\s{2,}(.*)$', line)
            chunks_count = len(self.subcols) // len(self.cols)
            values = list(chunk(chunks_count, m.group(2).split()))
            attrs[normalize_attr(m.group(1))] = values
        return attrs


class SipMethodTable(Table):
    """Базовый класс таблиц, содержащих данные по SIP-методам."""

    head_len = 4
    footer_len = 3
    cols = ('server', 'client')
    subcols = ('recent', 'total', 'permax', 'recent', 'total', 'permax')


class SipPeriodLifetimeTable(Table):
    """Базовый класс таблиц."""

    head_len = 3
    cols = ('period', 'lifetime')
    subcols = ('active', 'high', 'total', 'total', 'permax', 'high')


class SipLifetimeTable(Table):
    """Базовый класс таблиц."""

    head_len = 3
    cols = ('lifetime',)
    subcols = ('recent', 'total', 'permax')


class Status(Table):
    """show sipd status."""

    head_len = 3
    footer_len = 4
    cols = ('period', 'lifetime')
    subcols = ('active', 'high', 'total', 'total', 'permax', 'high')


class Policy(SipLifetimeTable):
    """show sipd policy."""


class Errors(SipLifetimeTable):
    """show sipd errors."""


class Server(SipPeriodLifetimeTable):
    """show sipd server."""


class Client(SipPeriodLifetimeTable):
    """show sipd client."""


class AclStatus(SipPeriodLifetimeTable):
    """show sipd acls."""

    def get_raw_lines(self, raw_data):
        """Возвращает массив сырых строк."""
        raw_lines = super(AclStatus, self).get_raw_lines(raw_data)
        sep = raw_lines.index('')
        return raw_lines[:sep]


class AclOperations(SipLifetimeTable):
    """show sipd acls."""

    head_len = 2

    def get_raw_lines(self, raw_data):
        """Возвращает массив сырых строк."""
        raw_lines = super(AclOperations, self).get_raw_lines(raw_data)
        sep = raw_lines.index('') + 1
        return raw_lines[sep:]


class Acls:
    """show sipd acls."""

    def __init__(self, data):
        """Init."""
        self.status = AclStatus(data)
        self.operations = AclOperations(data)


class Sessions(SipPeriodLifetimeTable):
    """show sipd sessions."""


class Invite(SipMethodTable):
    """show sipd invite."""


class Ack(SipMethodTable):
    """show sipd ack."""


class Bye(SipMethodTable):
    """show sipd bye."""


class Register(SipMethodTable):
    """show sipd register."""


class Cancel(SipMethodTable):
    """show sipd cancel."""


class Prack(SipMethodTable):
    """show sipd prack."""


class Options(SipMethodTable):
    """show sipd options."""


class Info(SipMethodTable):
    """show sipd info."""


class Subscribe(SipMethodTable):
    """show sipd subscribe."""


class Notify(SipMethodTable):
    """show sipd notify."""


class Refer(SipMethodTable):
    """show sipd refer."""


class Update(SipMethodTable):
    """show sipd update."""


class Registrations(SipPeriodLifetimeTable):
    """show registrations."""

    head_len = 2


class Sip:
    """Основной класс для работы с модулем. Все таблицы модуля доступны для
    инициализации как аттрибуты этого класса.
    """

    def __init__(self):
        """Init."""
        tables = [Status, Policy, Errors, Acls, Server, Client, Sessions,
                  Invite, Ack, Bye, Register, Cancel, Prack, Options, Info,
                  Subscribe, Notify, Refer, Update, Registrations]

        for table in tables:
            setattr(self, table.__name__.lower(), table)


sip = Sip()
