"""Helpers."""

import itertools
import re


def chunk(length, iterable, fillvalue=None):
    """Chunk."""
    return itertools.zip_longest(*[iter(iterable)]*length, fillvalue=fillvalue)


def normalize_attr(attr):
    """Нормализация имени аттрибута."""
    chars_to_replace = '<>-_/ '
    translator = str.maketrans({k: '_' for k in chars_to_replace})
    result = attr.translate(translator)
    result = re.sub('__+', '_', result)
    return result.lower()
