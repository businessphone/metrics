
"""Параметры Acme Packet SBC."""

from tornado.options import options


options.define('acme', default='acmesbc')
options.define('acme_node_1', default='0.0.0.0')
options.define('acme_node_2', default='0.0.0.0')
options.define('acme_port', default=23)
options.define('acme_password', default='acme')
