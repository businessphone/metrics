
"""Telnet клиент к SBC."""

import os

from core.telnet import Client as TelnetClient
from core.base import Collector
from core.base import Metric

from acme.sip import sip


class AcmeNodeIsNotActive(Exception):
    """Хост находится в режиме Standby. Данные с него могут быть неполными."""
    pass


class AcmeClient(Collector):
    """Клиент к Acme Packet SBC."""

    acme_host_file = os.path.join('/tmp', self.settings.acme)

    def get_cached_acme_host(self):
        """Возвращает кешированный адрес SBC."""
        with open(self.acme_host_file, 'r') as f:
            acme_host = f.read().strip()
        return acme_host

    def node_is_active(self, health):
        """Проверяет, активна ли сторона SBC."""
        return any(re.match(r'^\s+State\s+Active$', x) for x in health)

    def switch_acme_host(self, current_host):
        """Переключение на активную сторону SBC."""
        acme_host = {
            self.settings.acme_node_1: self.settings.acme_node_2,
            self.settings.acme_node_2: self.settings.acme_node_1
        }.get(current_host)

        with open(self.acme_host_file, 'w') as f:
            f.write(acme_host)

        return acme_host

    def get_data(self, host, port):
        """Возвращает данные с SBC."""
        telnet = TelnetClient(host, port)
        telnet.set_prompt(">")

        telnet.read_until("Password: ")
        telnet.write(self.settings.acme_password)
        telnet.read_until_prompt()

        telnet.write("show health")
        health = telnet.read_until_prompt()
        if self.node_is_active(health):
            # SIP Status
            telnet.write('show sipd status')
            self.sip_status = telnet.read_until_prompt()

            # SIP Register
            telnet.write('show sipd register')
            self.sip_register = telnet.read_until_prompt()

            # SIP Invite
            telnet.write('show sipd invite')
            self.sip_invite = telnet.read_until_prompt()

            # SIP Registrations
            telnet.write('show registrations')
            self.sip_registrations = telnet.read_until_prompt()
        else:
            telnet.close()
            raise AcmeNodeIsNotActive

        telnet.close()

    def process(self):
        """Обработка метрик."""
        acme_host = self.get_cached_acme_host()
        if not acme_host:
            acme_host = self.settings.acme_node_1

        try:
            self.get_data(acme_host, self.settings.acme_port)
        except AcmeNodeIsNotActive:
            acme_host = self.switch_acme_host(acme_host)
            self.get_data(acme_host, self.settings.acme_port)

        # Метрики SIP Status
        table = sip.status(self.sip_status)
        yield Metric(self.settings.hostname,
                     'acme.sessions',
                     table.sessions.period.active)
        yield Metric(self.settings.hostname,
                     'acme.media_sessions',
                     table.media_sessions.period.active)

        # Метрики SIP Register
        table = sip.register(self.sip_register)
        yield Metric(self.settings.hostname,
                     'acme.register.bad_request',
                     table._400_bad_request.server.recent)
        yield Metric(self.settings.hostname,
                     'acme.register.unauthorized',
                     table._401_unauthorized.server.recent)
        yield Metric(self.settings.hostname,
                     'acme.register.not_found',
                     table._404_not_found.server.recent)

        # Метрики SIP Invite
        table = sip.invite(self.sip_invite)
        yield Metric(self.settings.hostname,
                     'acme.invite.bad_request',
                     table._400_bad_request.server.recent)
        yield Metric(self.settings.hostname,
                     'acme.invite.unauthorized',
                     table._401_unauthorized.server.recent)
        yield Metric(self.settings.hostname,
                     'acme.invite.not_found',
                     table._404_not_found.server.recent)

        # Метрики SIP Registrations
        table = sip.registrations(self.sip_registrations)
        yield Metric(self.settings.hostname,
                     'acme.online',
                     table.user_entries.period.active)
