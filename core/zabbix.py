
"""Работа с Zabbix."""

import time
import json
import socket
import struct

from tornado.log import logging


class Zabbix:
    """Работа с Zabbix."""

    metrics = []
    logger = logging.getLogger('zabbix')

    def __init__(self, settings):
        """Init."""
        self.settings = settings

    def append(self, metric):
        """Добавляет метрику к запросу на Zabbix."""
        self.metrics.append(metric)

    def recive(self, sock, count):
        """Recive data back from socket."""
        buf = b''
        while len(buf) < count:
            chunk = sock.recv(count-len(buf))
            if not chunk:
                return buf
            buf += chunk
        return buf

    def send(self):
        """Отправка данных на сервер."""
        metrics_data = []
        for m in self.metrics:
            clock = m.clock or ('%d' % time.time())
            metrics_data.append(('{"host":%s,"key":%s,"value":%s,"clock":%s}') % (
                json.dumps(m.host), json.dumps(m.key), json.dumps(m.value), json.dumps(clock)))
        json_data = ('{"request":"sender data","data":[%s]}') % (','.join(metrics_data))
        json_data = bytes(json_data, 'utf-8')
        data_len = struct.pack('<Q', len(json_data))
        packet = b'ZBXD\x01' + data_len + json_data

        zabbix = socket.socket()
        zabbix.connect((self.settings.ZABBIX_HOST, self.settings.ZABBIX_PORT))
        zabbix.sendall(packet)

        resp_hdr = self.recive(zabbix, 13)
        if not resp_hdr.decode("utf-8").startswith('ZBXD\x01') or len(resp_hdr.decode("utf-8")) != 13:
            self.logger.error('Wrong zabbix response')
            return False
        resp_body_len = struct.unpack('<Q', resp_hdr[5:])[0]
        resp_body = zabbix.recv(resp_body_len)
        zabbix.close()

        resp = json.loads(resp_body.decode("utf-8"))

        if resp.get('response') != 'success':
            self.logger.error('Got error from Zabbix: %s' % resp)
            return False
        self.logger.debug('zabbix send data: success. %s' % resp['info'])
