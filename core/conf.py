
"""Основные методы."""

import os
import sys

from tornado.options import options, Error
from tornado.options import parse_command_line
from tornado.util import import_object

options.define("debug", default=False)
options.define("zabbix_path", default='/opt/zabbix/')
options.define("zabbix_host", default="127.0.0.1")
options.define("zabbix_port", default=10051)
options.define("hostname", default='zbx')
options.define("service", default='')
options.define("agent", default=False)
options.define("metric", default=None)


class Settings:
    """Конфигурация сервисов."""

    BASE_DIR = os.path.dirname(os.path.dirname(__file__))

    def __init__(self):
        """Инициализация настроек."""
        try:
            parse_command_line(args=[None] + [x for x in sys.argv if 'service' in x], final=False)
            self.service = options.service
        except Error:
            exit()

        if sys.argv and len(sys.argv) == 2 and sys.argv[1] == 'configure':
            self.service = 'configure'
        else:
            try:
                import_object("%s.config" % self.service)
                parse_command_line()
            except (ImportError, Error):
                exit()

        try:
            local_settings = import_object("core.local_settings")
            for key in dir(local_settings):
                if key in ['__doc__']:
                    continue
                if (getattr(self, key, None)):
                    setattr(options, key, getattr(local_settings, key))
        except (ImportError, Error):
            pass

    def __getattr__(self, name):
        """Возвращает значение настройки."""
        return getattr(options, name)

settings = Settings()
