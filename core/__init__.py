
"""Core module."""

import asyncio

coroutines = []


def coroutine(klass):
    """Coroutine."""
    fnc = klass()
    coroutines.append(asyncio.coroutine(fnc))
    return fnc
