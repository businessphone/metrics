"""Основные методы."""

from tornado.log import logging
from tornado.util import import_object
from tornado.options import parse_command_line
from tornado.log import enable_pretty_logging

import asyncio

from core.conf import settings
from core.zabbix import Zabbix
from core.etc import configure


class Metric:
    """Объект метрики."""

    def __init__(self):
        """Инициализация сервиса."""
        parse_command_line()

        if settings.debug:
            enable_pretty_logging()
        else:
            settings.logging = 'none'

        self.logger = logging.getLogger('core')
        self.logger.debug("Configure metric %s" % settings.service)

        if settings.service == 'configure':
            configure()
            return

        try:
            self.service = import_object("%s" % settings.service)
        except ImportError as e:
            self.logger.error("[%s] metric does not exist <%s>" % (settings.service, e))

    @asyncio.coroutine
    def run_metrics(self, loop, metrics):
        """Запуск метрик."""
        for metric_function in metrics:
            metrics = yield from metric_function(settings, loop)
            print(metrics.value)

    def start(self):
        """Старт основного процесса сервиса."""
        if settings.service == 'configure':
            return
        if settings.agent:
            metric = getattr(self.service, 'collect_%s' % settings.metric)
            loop = asyncio.get_event_loop()
            loop.run_until_complete(self.run_metrics(loop, [metric]))
        else:
            zabbix = Zabbix(settings)
            for metric in self.service.collect(settings):
                zabbix.append(metric)
            zabbix.send()

