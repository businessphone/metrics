
"""Базовые классы метрик."""


class Metric:
    """Базовая метрика."""

    def __init__(self, host, key, value, clock=None):
        """Инициализация метрики."""
        self.host = host
        self.key = key
        if type(value) is bool:
            value = value and 1 or 0
        self.value = value
        if value is None:
            print(key)
        self.clock = clock

    def __repr__(self):
        if self.clock is None:
            return 'Metric(%r, %r, %r)' % (self.host, self.key, self.value)
        return 'Metric(%r, %r, %r, %r)' % (self.host, self.key, self.value, self.clock)


class Collector:
    """Базовый класс для всех сборщиков метрик."""

    def process(self):
        """Обработка коллетора."""
        raise NotImplementedError()

    def __call__(self, settings):
        """Вызываем обработку."""
        self.settings = settings
        yield from self.process()
