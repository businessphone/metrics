
"""Работа с telnet соединениями."""

import socket


class TelnetError(Exception):
    """Exception."""


class Client:
    """Telnet client."""

    def __init__(self, host, port=23):
        """Инициализируем telnet client"""
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(2)

        # подключаемся к СБЦ
        try:
            self.socket.connect((host, port))
        except socket.error:
            raise TelnetError('Unable to connect')

    def stop_read(self):
        self.read = False

    def read_lines(self):
        """Производим чтение."""
        str_buffer = ""
        self.read = True
        while self.read:
            data = self.socket.recv(1000)
            try:
                data = data.decode("utf-8")
            except UnicodeDecodeError:
                continue
            str_buffer = "%s%s" % (str_buffer, data)

            if data and len(data) == 1000:
                continue

            for line in str_buffer.split("\n"):
                yield line

            str_buffer = ""

    def set_prompt(self, prompt):
        """Set prompt."""
        self.prompt = prompt

    def write(self, string):
        self.socket.send(bytes(string + "\n", 'utf-8'))

    def close(self):
        """Закрываем соединение."""
        self.socket.close()

    def read_until(self, string, end_with=False):
        """Производим чтение до появления в строке символа."""
        lines = []
        for line in self.read_lines():
            lines.append(line)
            if line == string or (end_with and line.strip() and line.strip().endswith(string)):
                self.stop_read()
                break
        if lines[0].strip() == "s":
            lines = lines[1:]
        return lines

    def read_until_prompt(self):
        """Производим чтение до появления промпта."""
        return self.read_until(self.prompt, True)
