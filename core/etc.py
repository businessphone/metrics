"""Etc."""

import os
import shutil

from tornado import template

from core.conf import settings


def render_config(path, file_name):
    """Render."""
    file_path = os.path.join(path, file_name)
    etc = os.path.join(settings.BASE_DIR, 'etc')
    tmp = os.path.join(settings.BASE_DIR, 'tmp/etc', path.replace(etc + '/', ''))

    try:
        os.makedirs(tmp)
    except OSError:
        pass

    new_file_path = os.path.join(tmp, file_name)

    loader = template.Loader(path)
    rendered = loader.load(file_name).generate(settings=settings)
    # rendered = render_to_string(file_path, {'settings': settings}, dirs=['/'])
    new = open(new_file_path, "wb+")
    new.write(rendered)
    new.close()


def update_configs(path):
    """Update configs."""
    for root, dirs, files in os.walk(path, topdown=False):
        for name in files:
            render_config(root, name)
        for name in dirs:
            update_configs(os.path.join(root, name))


def configure():
    """Configure."""
    etc = os.path.join(settings.BASE_DIR, 'etc')
    tmp = os.path.join(settings.BASE_DIR, 'tmp/etc')
    tmp_prev = os.path.join(settings.BASE_DIR, 'tmp/etc_prev')
    try:
        if os.path.isdir(tmp):
            if os.path.isdir(tmp_prev):
                shutil.rmtree(tmp_prev)
            shutil.move(tmp, tmp_prev)
    except Exception:
        pass
    update_configs(etc)
