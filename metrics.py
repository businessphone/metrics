
u"""Скрипт запуска метрики."""

from core import main


def init():
    """Инициализация метрики."""
    service = main.Metric()
    service.start()

if __name__ == '__main__':
    init()
