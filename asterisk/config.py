
u"""параметры asterisk."""

from tornado.options import options

options.define("asterisk_host", default="127.0.0.1")
options.define("asterisk", default="/opt/asterisk/sbin/asterisk")
