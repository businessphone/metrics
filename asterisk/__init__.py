
"""Asterisk metrics."""

import subprocess
import os
import re

import aiosip
import asyncio

from core.base import Metric


@asyncio.coroutine
def collect_status(settings, loop):
    """Collect asterisk status."""
    FNULL = open(os.devnull, 'w')
    if settings.debug:
        error_status = subprocess.call([settings.asterisk, "-x", "core show uptime"])
    else:
        error_status = subprocess.call([settings.asterisk, "-x", "core show uptime"], stdout=FNULL, stderr=subprocess.STDOUT)
    return Metric(settings.hostname, 'asterisk.status', not error_status)


@asyncio.coroutine
def collect_uptime(settings, loop):
    """Collect asterisk uptime."""
    FNULL = open(os.devnull, 'w')
    try:
        data = subprocess.check_output(
            [settings.asterisk, "-x", "core show uptime seconds"], stderr=FNULL)
        data = data.decode('utf-8')
        system_uptime = int(data.splitlines()[0].split()[-1])
    except subprocess.CalledProcessError:
        system_uptime = 0
    return Metric(settings.hostname, 'asterisk.uptime', system_uptime)


@asyncio.coroutine
def collect_avg_peer_latency(settings, loop):
    """Collect average peer latency."""
    data = subprocess.check_output([settings.asterisk, '-x', 'sip show peers'])
    _, *peers, _ = data.decode('utf-8').splitlines()
    regex = r'\w+\s+\((\d+)\s+ms\)'
    value = round(sum(int(re.search(regex, x).group(1)) for x in peers)/len(peers))
    return Metric(settings.hostname, 'asterisk.avg_peer_latency', value)


@asyncio.coroutine
def collect_sip_status(settings, loop):
    """Collect asterisk sip status."""
    app = aiosip.Application(loop=loop)
    sip_config = {
        'srv_host': settings.asterisk_host,
        'srv_port': '5060',
        'realm': 'onhook',
        'user': 'onhook',
        'pwd': 'onhook',
        'local_ip': '0.0.0.0',
        'local_port': 5062
    }
    dialog = yield from app.start_dialog(
        from_uri='sip:{user}@{srv_host}:{local_port}'.format(**sip_config),
        to_uri='sip:{user}@{srv_host}:{srv_port}'.format(**sip_config),
        local_addr=(sip_config['local_ip'], sip_config['local_port']),
        remote_addr=(sip_config['srv_host'], sip_config['srv_port']),
        password=sip_config['pwd'],
    )
    watched_user = 'onhook'
    send_future = dialog.send_message(
        method='OPTIONS',
        to_uri='sip:{0}@{realm}:{srv_port}'.format(watched_user, **sip_config),
        payload=''
    )
    try:
        result = yield from asyncio.wait_for(send_future, 0.1, loop=loop)
        return Metric(settings.hostname, 'asterisk.sip_status', True)
    except asyncio.TimeoutError:
        return Metric(settings.hostname, 'asterisk.sip_status', False)
