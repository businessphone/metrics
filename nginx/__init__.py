
u"""Метрики nginx сервера."""

import requests
import re
import os
import datetime
import time

import asyncio

from core.base import Metric


def parse_nginx_stat_data(data):
    """Получает данные по статистике nginx."""
    data = data.split('\n')
    result = {}
    # Active connections
    result['active_connections'] = re.match(r'(.*):\s(\d*)', data[0], re.M | re.I).group(2)
    # Accepts
    result['accepted_connections'] = re.match(r'\s(\d*)\s(\d*)\s(\d*)', data[2], re.M | re.I).group(1)
    # Handled
    result['handled_connections'] = re.match(r'\s(\d*)\s(\d*)\s(\d*)', data[2], re.M | re.I).group(2)
    # Requests
    result['handled_requests'] = re.match(r'\s(\d*)\s(\d*)\s(\d*)', data[2], re.M | re.I).group(3)
    # Reading
    result['header_reading'] = re.match(r'(.*):\s(\d*)(.*):\s(\d*)(.*):\s(\d*)', data[3], re.M | re.I).group(2)
    # Writing
    result['body_reading'] = re.match(r'(.*):\s(\d*)(.*):\s(\d*)(.*):\s(\d*)', data[3], re.M | re.I).group(4)
    # Waiting
    result['keepalive_connections'] = re.match(r'(.*):\s(\d*)(.*):\s(\d*)(.*):\s(\d*)', data[3], re.M | re.I).group(6)
    return result


@asyncio.coroutine
def collect_status(settings, loop):
    """status."""
    try:
        nginx_stat = requests.get(settings.NGINX_STAT_URL)
    except requests.exceptions.RequestException:
        return Metric(settings.hostname, 'nginx.status', False)
    nginx_stat_data = parse_nginx_stat_data(nginx_stat.text)
    return Metric(settings.hostname, 'nginx.ping', True)


def get_metrics_from_log(hostname, rps, codes_stat, stamp):
    u""" Возвращает метрики на основе данных полученных из лога нджинкса.  """
    d = stamp - datetime.timedelta(minutes=1)
    minute = int(time.mktime(d.timetuple()) / 60)*60

    # Adding the request per seconds to response
    for t in range(0, 60):
        yield Metric(hostname, 'nginx[rps]', rps[t], minute + t)

    # Adding the response codes stats to respons
    for t in codes_stat:
        yield Metric(hostname, 'nginx[%s]' % t, codes_stat[t])


def read_seek(file):
    if os.path.isfile(file):
        f = open(file, 'r')
        try:
            result = int(f.readline())
            f.close()
            return result
        except:
            return 0
    else:
        return 0


def write_seek(file, value):
    f = open(file, 'w')
    f.write(value)
    f.close()


def collect(settings):
    """
        Получаем метрики со страницы статуса.

        location /nginx_stat {
                stub_status on;       # Turn on nginx stats
                access_log   off;     # We do not need logs for stats
                #allow 127.0.0.1;      # Security: Only allow access from IP
                #deny all;             # Deny requests from the other of the world
            }
    """
    # Формируем метрики на основе урла статистики нджинкса

    if settings.NGINX_STAT_URL:
        try:
            nginx_stat = requests.get(settings.NGINX_STAT_URL)
        except requests.exceptions.RequestException:
            return [Metric(settings.HOSTNAME, 'nginx.ping', 0)]
        nginx_stat_data = parse_nginx_stat_data(nginx_stat.text)
        yield Metric(settings.HOSTNAME, 'nginx.ping', 1)

        for metric in nginx_stat_data:
            yield Metric(settings.HOSTNAME, 'nginx[%s]' % metric, nginx_stat_data[metric])

    if settings.NGINX_LOG_FILE:
        log_file = open(settings.NGINX_LOG_FILE, 'r')
        seek = read_seek(os.path.join(settings.BASE_DIR, 'tmp', 'nginx_log_stat'))
        new_seek = seek

        if os.path.getsize(settings.NGINX_LOG_FILE) > seek:
            log_file.seek(seek)

        # Initial rps
        rps = [0] * 60
        codes_stat = {}

        pat = (
            r''
            '(\d+.\d+.\d+.\d+)\s-\s-\s'  # IP address
            '\[(.+)\]\s'  # datetime
            '"GET\s(.+)\s\w+/.+"\s\d+\s'  # requested file
            '\d+\s"(.+)"\s'  # referrer
            '"(.+)"'  # user agent
        )

        minute = None
        stamp = None
        while True:
            new_seek = log_file.tell()
            line = log_file.readline()
            if not line:
                break
            match = re.findall(pat, line)

            if match:
                row = match[0]
                stamp = datetime.datetime.strptime(row[1].split(" ")[0], "%d/%b/%Y:%H:%M:%S")
                current = stamp.strftime("%d/%b/%Y:%H:%M")
                if minute and minute != current:
                    for metric in get_metrics_from_log(settings.HOSTNAME, rps, codes_stat, stamp):
                        yield metric
                    rps = [0] * 60
                    codes_stat = {}

                rps[stamp.second] += 1
                code = re.match(r'(.*)"\s(\d*)\s', line).group(2)
                if code not in codes_stat:
                    codes_stat[code] = 0
                codes_stat[code] += 1
        write_seek(os.path.join(settings.BASE_DIR, 'tmp', 'nginx_log_stat'), str(new_seek))
        if stamp:
            for metric in get_metrics_from_log(settings.HOSTNAME, rps, codes_stat, stamp):
                yield metric
