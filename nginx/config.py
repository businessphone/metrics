
u""" параметры nginx. """

from tornado.options import options

options.define("NGINX_STAT_URL", default="http://127.0.0.1/nginx_stat")
options.define("NGINX_LOG_FILE", default="/var/log/nginx/access.log")
